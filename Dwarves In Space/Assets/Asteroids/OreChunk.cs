using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class OreChunk : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Renderer which presents the ore sprite")]
    private SpriteRenderer asteroidRenderer;

    public float Weight => 150;

    /**
     * An ore type should not be a single color but something like "Iron", "Copper", etc.
     * This will do for now.
     */
    public Color OreType
    {
        get => oreType;
        set
        {
            oreType = value;
            asteroidRenderer.color = oreType;
        }
    }
    private Color oreType;

    protected virtual void Awake()
    {
        if (asteroidRenderer == null)
            asteroidRenderer = GetComponent<SpriteRenderer>();
    }
}
