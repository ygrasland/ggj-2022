using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropSite : MonoBehaviour
{
    /**
     * List of ore chunks that have been collected by the miner so far.
     */
    public List<Color> collectedMaterials;

    /**
     * Add an ore chunk to the content of this site. For now we store only colors,
     * later we will store actual data.
     */
    public void DepositOreChunk(OreChunk oreChunk)
    {
        Color oreType = oreChunk.OreType;
        collectedMaterials.Add(oreType);
        Destroy(oreChunk.gameObject);
        Debug.Log($"An ore chunk was collected, type is: {oreType}");
    }
}
