using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public class MinerController : MonoBehaviour
{

    [Tooltip("Miner root transform (default: this object).")]
    public Transform target;
    public float sensitivity = 10;

    /** Total mass moved by the thruster (character + gear + loadout) */
    public float totalMovedMass => characterMass + (IsCarryingOre ? CarriedOreChunk.Weight : 0);
    [Tooltip("Mass of the character and its gear (including the jetpack and fuel)")]
    public float characterMass = defaultCharacterMass;
    /**
     * Default mass of the character and its gear (including the jetpack and fuel).
     * Making the jetpack and gear heavy will make the relative weight change smaller
     * when the player picks up a big chunk of rock. This could be used to make the miner
     * somewhat bulkier when he's carrying rock.
     */
    private static readonly float defaultCharacterMass = 300;

    [Tooltip("Maximum thrust speed (no thrust is applied beyond this value)")]
    public float maximumThrustSpeed = 7;

    /** Normalized value of "thrustDirection" */
    public Vector2 normalizedThrustDirection => localThrustDirection.normalized;
    [Tooltip("Direction in which the jetpack pushes")]
    public Vector2 localThrustDirection = new Vector2(0.3f, 0.7f);

    private Vector3 currentSpeed;

    [Tooltip("Maximum forward thrust of the jetpack")]
    public float maxForwardThrust = defaultCharacterMass * defaultUnloadedAcceleration;
    /**
    * Default acceleration of the miner when he's not carrying anything apart from its
    * jetpack and gear.
    */
    private static readonly float defaultUnloadedAcceleration = 10;

    Vector3 WorldThrusterOrientation => transform.TransformDirection(localThrustDirection).normalized;

    public OreChunk CarriedOreChunk { get; private set; }
    public bool IsCarryingOre => CarriedOreChunk != null;

    protected virtual void Start()
    {
        if (!target)
        {
            target = transform;
        }
    }

    /** Stops all thruster movement */
    public void Stop()
    {
        currentSpeed = Vector3.zero;
    }

    public void CarryOreChunk(OreChunk chunk)
    {
        if (IsCarryingOre)
            throw new System.InvalidOperationException("At most one ore chunk can be carried at any given time");
        chunk.transform.parent = transform;
        chunk.transform.localPosition = new Vector3(0.15f, -0.15f, -1);
        chunk.transform.localRotation = Quaternion.identity;
        CarriedOreChunk = chunk;
    }

    public OreChunk DropCarriedChunk()
    {
        if (!IsCarryingOre)
            throw new System.InvalidOperationException("No ore chunk is being currently carried");
        OreChunk chunk = CarriedOreChunk;
        CarriedOreChunk = null;
        return chunk;
    }

    protected virtual void Update()
    {
        Vector3 desiredThrustDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        if (desiredThrustDirection != Vector3.zero)
        {
            /* Puissance de poussee */
            Vector3 newWorldThrusterOrientation = transform.TransformDirection(normalizedThrustDirection);
            Quaternion minerRotation = Quaternion.FromToRotation(newWorldThrusterOrientation, desiredThrustDirection);
            target.rotation = minerRotation * target.rotation;
        }

        float thrustPower = Input.GetAxis("Thrust power") * maxForwardThrust;
        if (thrustPower != 0)
        {
            /* Thrust */
            /* string DebugString = Enumerable.Range(1, 10).
                Where(index => Input.GetAxis($"A{index}") != 0).
                Select(index => $"Axis {index}: {Input.GetAxis($"A{index}")}").
                Aggregate("", (s1, s2) => $"{s1}, {s2}");
            if (!string.IsNullOrEmpty(DebugString))
            {
                Debug.Log(DebugString);
            }*/
            float speedInThrustDirection = Vector3.Dot(WorldThrusterOrientation, currentSpeed);
            if (speedInThrustDirection < maximumThrustSpeed)
            {
                float acceleration = thrustPower / totalMovedMass;
                currentSpeed += acceleration * WorldThrusterOrientation * Time.deltaTime;
            }
        }
        target.position += currentSpeed * Time.deltaTime;
    }
}
