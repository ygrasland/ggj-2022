using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinerRepositioner : MonoBehaviour
{
    [Tooltip("Target miner object")]
    public MinerController miner;

    private Vector3 initialPosition;
    private Quaternion initialRotation;

    protected virtual void Start()
    {
        if (!miner)
        {
            miner = GetComponent<MinerController>();
        }
        if (miner)
        {
            initialPosition = miner.transform.position;
            initialRotation = miner.transform.rotation;
        }
        else
        {
            Debug.LogWarning("This miner repositionner has no target at startup.");
        }
    }

    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button3)) {
            if (miner)
            {
                Debug.Log("Resetting miner position");
                miner.Stop();
                miner.transform.position = initialPosition;
                miner.transform.rotation = initialRotation;
            }
            else
            {
                Debug.LogError("This miner repositionner has no target; repositionning aborted.");
            }
        }
    }
}
