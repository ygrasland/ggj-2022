using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [Tooltip("Renderer which presents the asteroid sprite")]
    public SpriteRenderer asteroidRenderer;

    [Tooltip("Prefab of the ore chunks contained in this asteroid")]
    public OreChunk oreChunkPrefab;

    /** Color of the asteroid and contained mineral */
    private Color asteroidTint;

    /**
     * Mines a resource from this asteroid. This may destroy the object.
     */
    public OreChunk MineResource()
    {
        OreChunk newChunk = Instantiate<OreChunk>(oreChunkPrefab);
        newChunk.transform.position = transform.position;
        newChunk.OreType = asteroidTint;
        Destroy(gameObject);
        return newChunk;
    }

    protected virtual void Start()
    {
        if (asteroidRenderer == null)
            asteroidRenderer = GetComponent<SpriteRenderer>();
        asteroidTint = SampleAsteroidColor();
        asteroidRenderer.color = asteroidTint;
    }

    protected virtual Color SampleAsteroidColor()
    {
        // In the future this will be dictated by the kind or rarity 
        return new Color(
            0.5f + Random.value * 0.5f,
            0.5f + Random.value * 0.5f,
            0.25f + Random.value * 0.75f
        );
    }
}
