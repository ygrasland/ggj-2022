using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MinerController))]
public class OreCollection : MonoBehaviour
{
    public float mineRadius = .175f;

    private MinerController miner;

    protected virtual void Start()
    {
        miner = GetComponent<MinerController>();
    }

    protected virtual void Update()
    {
        if (miner.IsCarryingOre)
            TryDepositOre();
        else
            TryMineOre();
    }

    public bool TryDepositOre()
    {
        if (miner.IsCarryingOre && Input.GetButtonDown("Fire1"))
        {
            RaycastHit2D[] hitObjects = Physics2D.CircleCastAll(transform.position, mineRadius, Vector2.zero);
            DropSite dropSite = hitObjects.
                Select(hit => hit.collider.GetComponent<DropSite>()).
                Where(a => a != null).
                FirstOrDefault();

            if (dropSite)
            {
                dropSite.DepositOreChunk(miner.DropCarriedChunk());
                return true;
            }
        }
        return false;
    }

    public bool TryMineOre()
    {
        if (!miner.IsCarryingOre && Input.GetButtonDown("Fire1"))
        {
            Asteroid asteroid = Physics2D.CircleCastAll(transform.position, mineRadius, Vector2.zero).
                Select(hit => hit.collider.GetComponent<Asteroid>()).
                Where(a => a != null).
                FirstOrDefault();

            if (asteroid)
            {
                OreChunk chunk = asteroid.MineResource();
                miner.CarryOreChunk(chunk);
                return true;
            }
        }
        return false;
    }
}
